Svn2Git
=========

Scalable svn to git conversion tool.

Set the following variables for conversion:

 - svn_username: (Username of SVN User)
 - svn_password: (Password of SVN User)
 - git_username: (BitBucket Username)
 - git_password: (BitBucket Password)
 - git_repo_url: (IP Address + port # of BitBucket location)
 - project_key: (Existing project key in BitBucket)
 
 Add SVN repos to repo_list.txt file.
 Add known git author information to authors.txt file.